import axios from 'axios'
import { RPCServer } from './RPCServer'
import { CallResponse, ICallResponseDTO } from '../CallResponse'
import { CallRequest } from '../CallRequest'

describe('basic rpc gatewayServer', () => {
  it('can setup a basic http (http 1.1) gatewayServer and listen on a port', async () => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
      gatewayServer: {
        http: { bind: 'localhost', port: 6789 },
      },
    })

    server.registerHandler(
      { procedure: 'basic-gatewayServer-test', scope: 'global', version: '1' },
      async (request) => {
        return { code: 200, success: true }
      },
    )

    server.start()

    try {
      const { data, status } = await axios.post('http://localhost:6789', { procedure: 'basic-gatewayServer-test' })

      expect(status).toEqual(200)
      expect(data).toMatchObject({ code: 200, success: true })
    } finally {
      server.stop()

      await new Promise((r) => setTimeout(r, 1000))
    }
  })

  it('handles scope correctly for procedures', async () => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
      gatewayServer: {
        http: { bind: 'localhost', port: 6789 },
      },
    })

    server.registerHandler(
      { procedure: 'scope-test', scope: 'global', version: '1' },
      async (request) => {
        return new CallResponse(
          {
            code: 200,
            data: { scope: 'global' },
            success: true,
          },
          request,
        )
      },
    )

    server.registerHandler(
      { procedure: 'scope-test', scope: 'first', version: '1' },
      async (request: CallRequest) => {
        return new CallResponse(
          {
            code: 200,
            data: { scope: 'first' },
            success: true,
          },
          request,
        )
      },
    )

    server.registerHandler(
      { procedure: 'scope-test', scope: 'second', version: '1' },
      async (request) => {
        return new CallResponse(
          {
            code: 200,
            data: { scope: 'second' },
            success: true,
          },
          request,
        )
      },
    )

    server.start()

    try {
      const noScope = await axios.post(
        'http://localhost:6789',
        { procedure: 'scope-test' }
      )

      expect(noScope.data.data.scope).toEqual('global')

      const first = await axios.post(
        'http://localhost:6789',
        { procedure: 'scope-test', scope: 'first' }
      )

      expect(first.data.data.scope).toEqual('first')

      const second = await axios.post(
        'http://localhost:6789',
        { procedure: 'scope-test', scope: 'second' }
      )

      expect(second.data.data.scope).toEqual('second')
    } finally {
      server.stop()

      await new Promise((r) => setTimeout(r, 1000))
    }
  })

  it('handles version correctly for procedures', async () => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
      gatewayServer: {
        http: { bind: 'localhost', port: 6789 },
      },
    })

    server.registerHandler(
      { procedure: 'version-test', scope: 'global', version: '1' },
      async (request) => {
        return new CallResponse(
          {
            code: 200,
            data: { version: '1' },
            success: true,
          },
          request,
        )
      },
    )

    server.registerHandler(
      { procedure: 'version-test', scope: 'global', version: '2' },
      async (request) => {
        return new CallResponse(
          {
            code: 200,
            data: { version: '2' },
            success: true,
          },
          request,
        )
      },
    )

    server.start()

    try {
      const noVersionSpecified = await axios.post(
        'http://localhost:6789',
        { procedure: 'version-test' }
      )

      expect(noVersionSpecified.data.data.version).toEqual('1')

      const one = await axios.post(
        'http://localhost:6789',
        { procedure: 'version-test', version: '1' }
      )

      expect(one.data.data.version).toEqual('1')

      const two  = await axios.post(
        'http://localhost:6789',
        { procedure: 'version-test', version: '2' },
      )

      expect(two.data.data.version).toEqual('2')
    } finally {
      server.stop()

      await new Promise((r) => setTimeout(r, 1000))
    }
  })

  it('provides a "call" method to make internal RPC calls', async () => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
    })

    server.registerHandler(
      { procedure: 'local', scope: 'global', version: '1' },
      async (request) => {
        if (request.args) {
          return {
            code: 200,
            data: { version: '1' },
            success: true,
          }
        } else {
          return {
            code: 422,
            success: false,
          }
        }
      },
    )

    try {
      await server.call(
        {
          args: false,
          procedure: 'local',
          scope: 'global',
          version: '1',
        },
        'test',
      )
    } catch (e: any) {
      expect(e.code).toEqual(422)
      expect(e.success).toBeFalsy()
    }

    const { code, success } = await server.call(
      {
        args: true,
        procedure: 'local',
        scope: 'global',
        version: '1',
      },
      'test',
    )

    expect(code).toEqual(200)
    expect(success).toBeTruthy()
  })

  it('registerHandler() provides a "call" method to the handler as 2nd param', async () => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
    })

    server.registerHandler(
      { procedure: 'local_1', scope: 'global', version: '1' },
      async (request, call) => {
        expect(request.identity?.authorization).toEqual('123')

        // call another handler internally that returns the original request's identity
        const local2Response = await call({
          procedure: 'local_2',
          scope: 'global',
          version: '1',
        })

        return {
          code: 200,
          data: local2Response.data,
          success: true,
        }
      },
    )

    server.registerHandler(
      { internal: true, procedure: 'local_2', scope: 'global', version: '1' },
      async (request) => ({
        code: 200,
        data: request.identity,
        success: true,
      }),
    )

    const { data } = await server.call(
      {
        args: true,
        identity: {
          authorization: '123',
        },
        procedure: 'local_1',
        scope: 'global',
        version: '1',
      },
      'test',
    )

    expect(data).toMatchObject({
      authorization: '123',
    })
  })

  it('allows event listener', (done) => {
    const server = new RPCServer({
      displayName: 'test',
      ephemeralId: Math.random().toString(),
    })

    const requestInfo = { procedure: 'test', scope: 'test', version: '1' }

    server.registerHandler(requestInfo, async (request) => {
      throw new Error('some error')
    })

    const unsubscribed = server.on(
      RPCServer.events.handler_error,
      (payload) => {
        expect(payload.error).toBeInstanceOf(Error)
        done()
      },
    )

    server.call(requestInfo, 'test').catch()
  })
})
