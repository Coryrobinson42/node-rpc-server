import Ajv from 'ajv'
import { CallRequest, ICallRequestDescription } from '../../../CallRequest'
import {
  Handler,
  HandlerRegistrationInfo,
  HandlerRequestValidator,
} from '../../../Handler'
import { CallResponse } from '../../../CallResponse'
import { makeHandlerRequestKey } from './utils/make-handler-request-key'
import Debug from 'debug'
import { EventBus } from '../../../EventBus/EventBus'
import { RPCEventTopics } from '../../Events'

const ajv = new Ajv({
  coerceTypes: 'array',
  removeAdditional: true,
  useDefaults: true, // ie: convert { name: null } to { name: "" } if the the name=string type
})

const debug = Debug('rpc:LocalHandlerManager')

interface LocalHandlerManagerOptions {
  /**
   * Called when a registered handler is done processing a request
   * @param request {CallRequest}
   * @param startRequestReturnValue {any} If the onHandlerStartRequest returned anything it will be available in this param
   */
  onHandlerEndRequest?: (
    request: CallRequest,
    startRequestReturnValue?: any,
  ) => void
  /**
   * Called when a registered handler starts processing a request
   * @param request
   */
  onHandlerStartRequest?: (request: CallRequest) => any
  eventBus: EventBus
}

export interface LocalHandlerManager {
  checkCanHandleRequest(request: ICallRequestDescription): boolean

  registerHandler(request: HandlerRegistrationInfo, handler: Handler): void
}

export class LocalHandlerManager {
  private eventBus: EventBus

  constructor(readonly options: LocalHandlerManagerOptions) {
    this.eventBus = options.eventBus
  }

  private handlersByHandlerRequestKey: {
    [key: string]: { internalOnly: boolean; handler: Handler }
  } = {}

  private handlerRequestValidationsByHandlerRequestKey: {
    [key: string]: HandlerRequestValidator
  } = {}

  checkCanHandleRequest = (request: ICallRequestDescription): boolean => {
    return !!this.handlersByHandlerRequestKey[makeHandlerRequestKey(request)]
  }

  getRegisteredHandlers = (): string[] => {
    return Object.keys(this.handlersByHandlerRequestKey)
  }

  getSimilarRegisteredProcedures = (
    request: ICallRequestDescription,
  ): string => {
    const rgx = new RegExp(request.procedure)

    return Object.keys(this.handlersByHandlerRequestKey)
      .filter((key) => {
        return rgx.test(key)
      })
      .join(', ')
  }

  manageRequest = async (request: CallRequest): Promise<CallResponse> => {
    debug('manageRequest', request)
    let onHandlerStartRequestReturnedValue: any

    if (this.options.onHandlerStartRequest) {
      onHandlerStartRequestReturnedValue =
        this.options.onHandlerStartRequest(request)
    }

    try {
      const key = makeHandlerRequestKey(
        CallRequest.toCallRequestDescription(request),
      )

      if (
        this.handlersByHandlerRequestKey[key].internalOnly &&
        !request.trace.internal
      ) {
        debug(
          'handler is marked "internalOnly" and the request is not coming from internal',
          request,
        )

        throw new CallResponse(
          {
            code: 403,
            message: 'caller is not authorized to access this procedure',
            success: false,
          },
          request,
        )
      }

      if (this.handlerRequestValidationsByHandlerRequestKey[key]) {
        this.handlerRequestValidationsByHandlerRequestKey[key](request)
      }

      return await this.handlersByHandlerRequestKey[key].handler(request)
    } catch (e: any) {
      if (e instanceof CallResponse) {
        debug('manageRequest catching a thrown CallResponse')

        return e as CallResponse
      }

      this.eventBus.publish({
        payload: {
          error: e,
          request,
        },
        topic: RPCEventTopics.handler_error,
      })

      console.log('LocalHandlerManager error: catching and returning 500', e)

      return new CallResponse(
        {
          code: 500,
          success: false,
        },
        request,
      )
    } finally {
      if (this.options.onHandlerEndRequest) {
        this.options.onHandlerEndRequest(
          request,
          onHandlerStartRequestReturnedValue,
        )
      }
    }
  }

  registerHandler = (request: HandlerRegistrationInfo, handler: Handler) => {
    debug('registerHandler', request)

    const key = makeHandlerRequestKey(request)

    this.handlersByHandlerRequestKey[key] = {
      internalOnly: !!request.internal,
      handler,
    }

    // Check if the HandlerRegistrationInfo has "args" as JSON-schema for automatic validation
    if (typeof request.args === 'object' && request.args?.type) {
      try {
        const validate = ajv.compile(request.args)

        this.handlerRequestValidationsByHandlerRequestKey[key] = (request) => {
          const valid = validate(request.args || {})

          if (!valid) {
            debug('validation error', validate.errors)

            throw new CallResponse(
              {
                code: 422,
                message: `"args" validation failed: ${validate.errors
                  ?.map((errorObj) => errorObj.instancePath + errorObj.message)
                  .join(', ')}`,
                success: false,
              },
              request,
            )
          }

          return undefined
        }
      } catch (e: any) {
        console.log(``)
        console.log(`ERROR: Unable to compile JSON-schema for "${key}"`)
        console.log(`   ${e.message}`)
        console.log(``)
        process.exit(1)
      }
    }
  }
}
