import WsServer from 'ws'
import { DEFAULT_BIND, Server } from './Server'
import { Handler } from '../../Handler'
import { RPCServerConfig } from '../RPCServer'
import { EventBus } from '../../EventBus/EventBus'
import Debug from 'debug'
import { CallRequest, ICallRequestDTO } from '../../CallRequest'
import { CallResponse } from '../../CallResponse'
import { RPCClientIdentity, isIdentityValid } from '../../RPCClientIdentity'
import { RPCEventTopics } from '../Events'
import { IncomingMessage } from 'http'
import uuid from 'node-uuid'
import WebSocket from 'ws'

const debug = Debug('rpc:WebSocketServer')

interface ExtendedWebSocket extends WebSocket {
  id: string
}

interface WebSocketServerOptions {
  config: RPCServerConfig
  eventBus: EventBus
}

export class WebSocketServer implements Server {
  private bind: string
  private eventBus: EventBus
  private port: number
  private requestHandler?: Handler
  private wss?: WsServer.Server

  constructor(readonly options: WebSocketServerOptions) {
    if (!options.config.gatewayServer?.websocket?.port) {
      throw new Error('config.gatewayServer.websocket.port is required')
    }

    this.bind = options.config.gatewayServer?.websocket?.bind || DEFAULT_BIND
    this.eventBus = options.eventBus
    this.port = options.config.gatewayServer?.websocket?.port
  }

  name = 'WebSocketServer'

  private static parseIPAddressFromWsReq(req: IncomingMessage): string {
    let ip = ''

    if (req.headers['x-real-ip']) {
      if (Array.isArray(req.headers['x-real-ip'])) {
        ip = req.headers['x-real-ip'][0]
      } else {
        ip = req.headers['x-real-ip'].split(',')[0]
      }
    } else {
      ip = req.socket.remoteAddress || ''
    }

    ip = ip.trim()

    return ip
  }

  public setIncomingRequestHandler = (requestHandler: Handler) => {
    this.requestHandler = requestHandler
  }

  start = (onStartedCallback?: () => void) => {
    if (!this.requestHandler) {
      throw new Error(`No request handler has be set`)
    }

    this.wss = new WsServer.Server({
      host: this.bind,
      port: this.port,
    })

    this.wss.on('connection', (ws: ExtendedWebSocket, req) => {
      const websocketId = uuid.v4()

      debug('websocket connected, connectionId:', websocketId)

      let identity: RPCClientIdentity | undefined
      const wsIp = WebSocketServer.parseIPAddressFromWsReq(req)

      if (this.options.config?.gatewayServer?.websocket?.onClientConnect) {
        this.options.config.gatewayServer.websocket.onClientConnect({
          connectionId: websocketId,
          identity,
          ip: wsIp,
        })
      }

      ws.on('close', (code, data) => {
        const reason = data.toString()

        debug('websocket disconnected, connectionId:', websocketId, reason)

        if (this.options.config?.gatewayServer?.websocket?.onClientDisconnect) {
          this.options.config.gatewayServer.websocket.onClientDisconnect({
            connectionId: websocketId,
            identity,
            ip: wsIp,
          })
        }
      })

      ws.on('message', async (data: WsServer.Data, isBinary: boolean) => {
        const message = isBinary ? data : data.toString()

        debug('received message from WS, connectionId:', websocketId, message)

        let messageParsed

        if (typeof message === 'string') {
          try {
            messageParsed = JSON.parse(message)
          } catch (e: any) {
            debug(
              'WebSocketServer error: received unparseable WS msg string',
              message,
            )

            return
          }
        } else {
          debug(
            'WebSocketServer error: received unknown "message" from client',
            message,
          )
        }

        const requestDTO: ICallRequestDTO = messageParsed

        if (requestDTO.identity) {
          if (!isIdentityValid(requestDTO.identity)) {
            console.log(
              'error recieving CallResponseDTO with invalid identity schema',
              requestDTO.identity,
            )

            return
          }

          if (
            identity?.authorization &&
            identity.authorization !== requestDTO.identity.authorization
          ) {
            console.log(
              'error, a websocket changed identity.authorization from previous value, disconnecting...',
            )

            ws.send(
              JSON.stringify(
                CallResponse.toCallResponseDTO(
                  new CallResponse(
                    {
                      code: 400,
                      message:
                        'identity.authorization has changed - disconnecting websocket connection',
                      success: false,
                    },
                    CallRequest.fromCallRequestDTO(
                      {
                        ...requestDTO,
                        // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                        procedure: '__internal-identity__',
                      },
                      { trace: { caller: 'WebSocketServer' } },
                    ),
                  ),
                ),
              ),
              () => ws.close(),
            )

            return
          }

          // If setting identity for the first time, only call the onClientConnect() cb once with the client's RPCClientIdentity
          if (!identity?.authorization && requestDTO.identity?.authorization) {
            if (
              this.options.config?.gatewayServer?.websocket?.onClientConnect
            ) {
              this.options.config.gatewayServer.websocket.onClientConnect({
                connectionId: websocketId,
                identity: requestDTO.identity,
                ip: wsIp,
              })
            }
          }

          identity = requestDTO.identity
        }

        // This is just a "set my identity" msg from the client
        if (!requestDTO.procedure && requestDTO.identity) {
          try {
            const response = new CallResponse(
              {
                code: 200,
                correlationId: requestDTO.correlationId,
                success: true,
              },
              CallRequest.fromCallRequestDTO(
                {
                  ...requestDTO,
                  // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                  procedure: '__internal-set-identity__',
                },
                { trace: { caller: 'WebSocketServer' } },
              ),
            )

            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)))
          } catch (e: any) {
            console.log(
              'error sending CallResponseDTO to WS connection after setting RPCClientIdentity, error:',
              e,
            )
          }

          return
        }

        let request: CallRequest | undefined

        try {
          request = CallRequest.fromCallRequestDTO(
            { identity, ...requestDTO },
            { trace: { caller: 'WebSocketServer', ipAddress: wsIp } },
          )
        } catch (e: any) {
          debug(
            'WebSocketServer error: WS msg not a valid CallRequestDTO',
            request,
            e,
          )

          return
        }

        if (request) {
          debug('handling received request')

          if (request.identity) {
            if (!isIdentityValid(request.identity)) {
              console.log(
                'error recieving CallResponseDTO with invalid RPCClientIdentity schema',
                request.identity,
              )

              return
            }

            identity = request.identity
          }

          const response = await this.handleIncomingRequest(request)

          debug('returning request response')

          try {
            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)))
          } catch (e: any) {
            console.log(
              'WebSocketServer error: error sending CallResponseDTO to WS connection',
              response,
              e,
            )

            this.eventBus.publish({
              payload: {
                error: e,
                request,
                response,
              },
              topic: RPCEventTopics.rpc_server_error,
            })
          }

          return
        }
      })
    })

    this.wss.once('listening', () => {
      debug(
        `${this.options.config.displayName} listening ${this.bind}:${this.port}`,
      )

      if (onStartedCallback) {
        onStartedCallback()
      }
    })
  }

  stop = (onStoppedCallback?: () => void) => {
    this.wss?.close((err) => {
      if (err) {
        console.log('WSS error onclose:', err)
      }
      if (onStoppedCallback) onStoppedCallback()
    })

    for (const ws of this.wss?.clients ?? []) {
      ws.terminate()
    }
  }

  private handleIncomingRequest = async (
    request: CallRequest,
  ): Promise<CallResponse> => {
    try {
      debug('sending request to requestHandler')

      const response: CallResponse = await this.requestHandler!(
        request as CallRequest,
      )

      return response
    } catch (e: any) {
      if (e instanceof CallResponse) {
        debug('handleIncomingRequest catching a throw CallResponse')

        return e as CallResponse
      }

      this.eventBus.publish({
        payload: {
          error: e,
          request,
        },
        topic: RPCEventTopics.rpc_server_error,
      })

      console.log(
        'WebSocketServer error: there was an error handling request:',
        request,
        e,
      )

      return new CallResponse(
        {
          code: 500,
          message: 'There was a problem calling the procedure',
          success: false,
        },
        request,
      )
    }
  }
}
