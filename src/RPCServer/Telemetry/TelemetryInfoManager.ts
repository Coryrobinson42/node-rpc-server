import { ICallRequestDescription } from '../../CallRequest'
import { HandlerRegistrationInfo } from '../../Handler'
import { RPCServerConfig } from '../RPCServer'
import { EventBus } from '../../EventBus/EventBus'
import { ITelemetryHeartbeat } from '../../Telemetry'
import os from 'os'
import { TelemetryPersistenceManager } from '../../TelemetryPersistence/TelemetryPersistenceManager'
import Timeout = NodeJS.Timeout
import { RPCEventTopics } from '../Events'

interface ITelemetryInfoManagerOptions {
  config: RPCServerConfig
  eventBus: EventBus
}

export class TelemetryInfoManager {
  private heartbeatData: ITelemetryHeartbeat = {
    hostName: os.hostname(),
    ephemeralId: '',
    gatewayHttpServer: false,
    gatewayWebSocketServer: false,
    displayName: '',
    startTime: '',
  }
  private heartbeatInterval?: Timeout

  private registeredHandlers: HandlerRegistrationInfo[] = []

  private statistics = {
    gateway: {
      http: {
        receivedCount: 0,
      },
      websocket: {
        receivedCount: 0,
      },
    },
    localHandlers: {
      errorCountByProcedure: {},
      errorCount: 0,
      handledCount: 0,
      handledCountByProcedure: {},
    },
    messageBroker: {
      receivedCount: 0,
      sentCount: 0,
    },
    handlerErrorCount: 0,
    handlerSuccessCount: 0,
    rpcServerErrorCount: 0,
  }

  private telemetryPersistenceManager: TelemetryPersistenceManager

  constructor(readonly options: ITelemetryInfoManagerOptions) {
    if (!options.config.telemetry?.adapter) {
      throw new Error('config.telemetry.adapter must be set')
    }

    this.telemetryPersistenceManager = new TelemetryPersistenceManager(
      options.config.telemetry?.adapter,
    )

    this.heartbeatData.ephemeralId = options.config.ephemeralId
    this.heartbeatData.gatewayHttpServer = !!options.config.gatewayServer?.http
    this.heartbeatData.gatewayWebSocketServer = !!options.config.gatewayServer
      ?.websocket
    this.heartbeatData.displayName = options.config.displayName
    this.heartbeatData.startTime = new Date().toISOString()

    this.setupEventBusListeners()
  }

  getHeartbeatData = () => this.heartbeatData

  getRegisteredHandlers = () => this.registeredHandlers

  getStatistics = () => this.statistics

  registerHandler = (request: ICallRequestDescription) => {
    this.registeredHandlers.push(request)
  }

  startTelemetryReporting = async () => {
    this.heartbeatInterval = setInterval(() => {
      this.saveHeartbeat()
    }, 30000)

    await this.saveHeartbeat()

    await this.telemetryPersistenceManager.saveHandlersRegistrationInfo(
      this.registeredHandlers.map((request) => ({
        ...request,
        createdAt: new Date().toISOString(),
        serverDisplayName: this.options.config.displayName,
      })),
    )
  }

  stopTelemetryReporting = () => {
    if (this.heartbeatInterval) {
      clearInterval(this.heartbeatInterval)
    }

    this.telemetryPersistenceManager.disconnect()
  }

  private setupEventBusListeners = () => {
    this.options.eventBus.subscribe<any>(
      RPCEventTopics.handler_call_success,
      (payload: any) => {
        this.statistics.handlerSuccessCount++
      },
    )

    this.options.eventBus.subscribe<any>(
      RPCEventTopics.handler_error,
      (payload: any) => {
        this.statistics.handlerErrorCount++
      },
    )

    this.options.eventBus.subscribe<any>(
      RPCEventTopics.rpc_server_error,
      (payload: any) => {
        this.statistics.rpcServerErrorCount++
      },
    )
  }

  private saveHeartbeat = async () => {
    return this.telemetryPersistenceManager.saveHeartbeat(this.heartbeatData)
  }

  private saveStatistics = () => {
    this.telemetryPersistenceManager.saveStatistics(this.statistics)
  }
}
