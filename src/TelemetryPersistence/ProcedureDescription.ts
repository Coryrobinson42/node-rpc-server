export interface ProcedureDescription {
  args?: any
  createdAt: string
  data?: any
  description?: string
  identity?: string[] | string
  internal?: boolean
  procedure: string
  scope: string
  serverDisplayName: string
  version: string
}
