import mongoose, { Schema } from 'mongoose'

export const HeartbeatModel = mongoose.model(
  'Heartbeat',
  new mongoose.Schema({
    // these documents will expire after 1min
    createdAt: { default: Date.now, type: Date, expires: 60 },
    hostName: String,
    ephemeralId: String,
    gatewayHttpServer: Boolean,
    gatewayWebSocketServer: Boolean,
    displayName: String,
    startTime: String,
  }),
)

export const ProcedureDescriptionModel = mongoose.model(
  'ProcedureDescription',
  new mongoose.Schema({
    args: Schema.Types.Mixed,
    createdAt: String,
    data: Schema.Types.Mixed,
    description: String,
    identity: [String],
    internal: Boolean,
    procedure: { required: true, type: String },
    scope: { default: 'global', type: String },
    serverDisplayName: String,
    version: { default: '1', type: String },
  }),
)
