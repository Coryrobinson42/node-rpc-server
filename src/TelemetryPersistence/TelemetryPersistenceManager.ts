import { uniqBy } from 'lodash'
import { ITelemetryHeartbeat } from '../Telemetry'
import {
  HeartbeatModel,
  ProcedureDescriptionModel,
} from './intrinsic/MongoDb/Models'
import { MongoManager } from './intrinsic/MongoDb/MongoManager'
import { ProcedureDescription } from './ProcedureDescription'

export class TelemetryPersistenceManager {
  private mongoManager: MongoManager

  constructor(adaptorConfig: { mongoURI: string }) {
    this.mongoManager = new MongoManager(adaptorConfig.mongoURI)
    this.mongoManager.setupMongoConnection()
  }

  disconnect = () => {
    this.mongoManager.tearDownConnection()
  }

  getHeartbeats = async (): Promise<any> => {
    return HeartbeatModel.find({}, null, { limit: 500 })
      .lean()
      .exec()
      .then((docs) => {
        return uniqBy(docs, 'ephemeralId')
      })
  }

  getProcedureDocs = async (): Promise<any> => {
    return ProcedureDescriptionModel.find({}, null, { limit: 5000 })
      .lean()
      .exec()
  }

  saveHeartbeat = async (telemetryHeartbeat: ITelemetryHeartbeat) => {
    const doc = new HeartbeatModel(telemetryHeartbeat)

    doc.save((err: any) => {
      if (err)
        return console.error(
          'TelemetryPersistenceManager#saveHeartbeat error:',
          err,
        )
    })
  }

  saveHandlersRegistrationInfo = async (
    procedureDescriptions: ProcedureDescription[],
  ) => {
    if (!procedureDescriptions.length) return

    let waitCount = 0

    while (!this.mongoManager.connected) {
      if (waitCount > 30) {
        throw Error('MongoManager has not connected to mongoDB')
      }

      waitCount++
      await new Promise((r) => setTimeout(r, 100))
    }

    try {
      const serverDisplayName = procedureDescriptions[0].serverDisplayName

      await ProcedureDescriptionModel.deleteMany({ serverDisplayName })

      await ProcedureDescriptionModel.insertMany(procedureDescriptions)
    } catch (err) {
      console.error(
        'TelemetryPersistenceManager#saveHandlerRegistrationInfo error:',
        err,
      )
    }
  }

  saveStatistics = async (telemetryStatistics: any) => {}
}
