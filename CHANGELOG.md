## [2.4.6](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.5...v2.4.6) (2022-02-15)


### Bug Fixes

* remove console.log that @Tom left in the code. ([dd20865](http://bitbucket.org/thermsio/rpc-server-ts/commits/dd208654b8e04002736909b911d0f7aba0f4abbb))

## [2.4.5](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.4...v2.4.5) (2022-02-11)


### Bug Fixes

* forwarded ip address header lowercase getter ([be82789](http://bitbucket.org/thermsio/rpc-server-ts/commits/be82789298626dc95fb016ce90f26081bde99b6f))

## [2.4.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.3...v2.4.4) (2022-02-10)


### Bug Fixes

* remote ip header identifier x-real-ip ([1dcb265](http://bitbucket.org/thermsio/rpc-server-ts/commits/1dcb265b2f4077bcd2b15e56a7aede6f5e9fc9c9))

## [2.4.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.2...v2.4.3) (2022-02-10)


### Bug Fixes

* koa.js HTTP gateway server proxy for ip forward ([c7fad74](http://bitbucket.org/thermsio/rpc-server-ts/commits/c7fad74373698452a4e7feb60a68a59d13ed8bac))

## [2.4.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.1...v2.4.2) (2022-02-10)


### Bug Fixes

* CallRequest.fromCallRequestDTO set ipAddress on returned value ([503301f](http://bitbucket.org/thermsio/rpc-server-ts/commits/503301f0a79a76aa091d10e25069b8456e4d2b80))

## [2.4.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.4.0...v2.4.1) (2022-02-10)


### Bug Fixes

* CallRequest.tract type ([de38c4c](http://bitbucket.org/thermsio/rpc-server-ts/commits/de38c4c528f17e8c9bcca3296618620f2248b5ce))

# [2.4.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.5...v2.4.0) (2022-02-10)


### Features

* add trace.ipAddress value CallRequest from gateway servers ([c3efa71](http://bitbucket.org/thermsio/rpc-server-ts/commits/c3efa7176357a9761b3536e47de9c6d850701fd2))

## [2.3.5](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.4...v2.3.5) (2022-01-21)


### Bug Fixes

* CORS origin:* ([c9c8266](http://bitbucket.org/thermsio/rpc-server-ts/commits/c9c8266dc559ae05787d9830f65d3cecffca5492))

## [2.3.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.3...v2.3.4) (2022-01-09)


### Bug Fixes

* add instancePath to ajv validation error on response ([e232e4e](http://bitbucket.org/thermsio/rpc-server-ts/commits/e232e4ef791802a94c54b8f0d98e2bbe62896f56))

## [2.3.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.2...v2.3.3) (2022-01-09)


### Bug Fixes

* add better debug logs ([c720a45](http://bitbucket.org/thermsio/rpc-server-ts/commits/c720a454f3623c2ef6f3451f341acd89d0ffd9dd))
* update deps, mongodb ts 4.3.0 ([addbb11](http://bitbucket.org/thermsio/rpc-server-ts/commits/addbb11306b0bce7ba4ef931df2c191bb064703f))

## [2.3.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.1...v2.3.2) (2022-01-06)


### Bug Fixes

* validation errors no longer return errors in CallResponseDTO  prop and only send error str in  prop ([24bd5f0](http://bitbucket.org/thermsio/rpc-server-ts/commits/24bd5f02e33da40a1d161966c4e8ac28699a3c47))

## [2.3.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.3.0...v2.3.1) (2021-12-27)


### Bug Fixes

* add generics to registerHandler() ([72b7d0d](http://bitbucket.org/thermsio/rpc-server-ts/commits/72b7d0dca02d5260ecc73427030464f31e2eb07f))

# [2.3.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.5...v2.3.0) (2021-12-26)


### Features

* add types for RPCServerHandler to achieve types and type completion inside a handler ([edc0625](http://bitbucket.org/thermsio/rpc-server-ts/commits/edc06254300b6dc85af34759de5711af609d9460))

## [2.2.5](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.4...v2.2.5) (2021-12-25)


### Bug Fixes

* update deps ([ff4f74f](http://bitbucket.org/thermsio/rpc-server-ts/commits/ff4f74f80a3a52d68c446d5b8873af0647e620bd))

## [2.2.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.3...v2.2.4) (2021-12-08)


### Bug Fixes

* TS types on Telemetry MongoManager ([de4d2f1](http://bitbucket.org/thermsio/rpc-server-ts/commits/de4d2f16a681a7d9642753815f77f4bfe4f6b99f))
* update major deps, mongodb@4 mongoos@6 ([6448bd0](http://bitbucket.org/thermsio/rpc-server-ts/commits/6448bd0493a89dea8e86f498a224e59cfff51ae1))

## [2.2.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.2...v2.2.3) (2021-12-07)


### Bug Fixes

* websocket disconnect send websocket connection ID ([4cf0a64](http://bitbucket.org/thermsio/rpc-server-ts/commits/4cf0a649fcedcfa3e2b9c6f4be988d90bdd90c4f))

## [2.2.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.1...v2.2.2) (2021-11-25)


### Bug Fixes

* node-uuid deps ([4442bed](http://bitbucket.org/thermsio/rpc-server-ts/commits/4442bedd45afdca58621d67774bd2aeb5dd9ed76))

## [2.2.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.2.0...v2.2.1) (2021-11-25)


### Bug Fixes

* WebSocketServer client disconnect when identity changes and sends a ws msg explaining ([b4abaec](http://bitbucket.org/thermsio/rpc-server-ts/commits/b4abaec98adf5306983fa76cb38ec046f42b6787))

# [2.2.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.1.0...v2.2.0) (2021-11-25)


### Features

* add connectionId prop to websocket connect/disconnect events ([15939b1](http://bitbucket.org/thermsio/rpc-server-ts/commits/15939b1af5230d71c610f77bb6b51656f8e3db26))

# [2.1.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.0.2...v2.1.0) (2021-10-25)


### Features

* callbacks for onClientConnect/Disconnect websockets ([c658ef8](http://bitbucket.org/thermsio/rpc-server-ts/commits/c658ef83f919f0e4bc0cc5879f008b25a3b61677))

## [2.0.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.0.1...v2.0.2) (2021-10-11)


### Bug Fixes

* build change TS module to ESNext ([a2156d3](http://bitbucket.org/thermsio/rpc-server-ts/commits/a2156d3a287793be3ca6074090ff020068fc46d3))

## [2.0.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v2.0.0...v2.0.1) (2021-10-07)


### Bug Fixes

* return better debug msg when a handler is not found for a call ([1139a99](http://bitbucket.org/thermsio/rpc-server-ts/commits/1139a99e478561c572f727148c79c982c48c4cc3))

# [2.0.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.17.0...v2.0.0) (2021-10-01)


### Code Refactoring

* rollup plugins ([7ef5862](http://bitbucket.org/thermsio/rpc-server-ts/commits/7ef58623ce7d94596d60bac96f64396b839250f3))


### BREAKING CHANGES

* bundling deps has changed, this might break previous use of the package

# [1.17.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.16.3...v1.17.0) (2021-09-08)


### Bug Fixes

* pin typescript at 4.3.5 because of this issue that causes rollup to hang forever https://github.com/microsoft/TypeScript/issues/45633 ([235cf21](http://bitbucket.org/thermsio/rpc-server-ts/commits/235cf21fd736f20aaa40f2b606c4540d0199f1fd))
* **HttpServer:** add error catch in koa http server ([6ea878e](http://bitbucket.org/thermsio/rpc-server-ts/commits/6ea878e95239a2d4ae5007827bc2e81854dce701))


### Features

* update deps and fix tests ([4fd3829](http://bitbucket.org/thermsio/rpc-server-ts/commits/4fd38294f9841fec9e14a3f588d4f799526141e6))

## [1.16.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.16.2...v1.16.3) (2021-07-15)


### Bug Fixes

* validate RPC handler when empty incoming RPC has no args object ([80e7d2b](http://bitbucket.org/thermsio/rpc-server-ts/commits/80e7d2b275a7d6ade58b1b1ab1cbdedf9f3eff75))

## [1.16.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.16.1...v1.16.2) (2021-06-21)


### Bug Fixes

* update deps ([496b0c7](http://bitbucket.org/thermsio/rpc-server-ts/commits/496b0c70413f238f0b4f2ab6a78a7ee7570c8c70))

## [1.16.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.16.0...v1.16.1) (2021-06-08)


### Bug Fixes

* bump major version deps ([f987b04](http://bitbucket.org/thermsio/rpc-server-ts/commits/f987b04a31df4e735b363a87cd9adfd1bd02407c))
* tests for jest v27 compat ([2cd50ba](http://bitbucket.org/thermsio/rpc-server-ts/commits/2cd50ba3ac8e80de5981d830caa2089e4bcab51f))

# [1.16.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.15.1...v1.16.0) (2021-05-25)


### Features

* getRegisteredHandlers ([77f5ebc](http://bitbucket.org/thermsio/rpc-server-ts/commits/77f5ebc617a315b95a428530ea994bc7a912c4e0))

## [1.15.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.15.0...v1.15.1) (2021-05-21)


### Bug Fixes

* add RPCServerHandler type for TS ([0967664](http://bitbucket.org/thermsio/rpc-server-ts/commits/0967664c82a0e17a9f7f0d114c2708b651d2c191))
* exports include RPCServerHandler ([c43a618](http://bitbucket.org/thermsio/rpc-server-ts/commits/c43a6188e2f7ec439a71875457e1eada28fc1f15))

# [1.15.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.14.0...v1.15.0) (2021-05-08)


### Features

* add options onHandlerEndRequest & onHandlerStartRequest for tracing requests time ([4c811d3](http://bitbucket.org/thermsio/rpc-server-ts/commits/4c811d3f4fb946c438ef1eab35f0d35373423705))

# [1.14.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.13.0...v1.14.0) (2021-05-08)


### Features

* error events published on event bus for handlers and internal errors ([302bdaa](http://bitbucket.org/thermsio/rpc-server-ts/commits/302bdaa9a48c346b7e04522230e4e4f8a4daa6c8))

# [1.13.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.12.4...v1.13.0) (2021-05-06)


### Bug Fixes

* internal trace caller name ([b9743bb](http://bitbucket.org/thermsio/rpc-server-ts/commits/b9743bb0e74d7c534a2a8767d38a1fec42684fbb))


### Features

* internal only calls ([42bde0e](http://bitbucket.org/thermsio/rpc-server-ts/commits/42bde0eccc0a1a5055586db9170a7deb0c2ac946))

## [1.12.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.12.3...v1.12.4) (2021-05-06)


### Bug Fixes

* useCreateIndexes mongoose ([cf19c6d](http://bitbucket.org/thermsio/rpc-server-ts/commits/cf19c6d4b754c1623ad7a3bf1a6541ae9d6e4bbf))
* **HeartbeatModel:** expire default ([052325c](http://bitbucket.org/thermsio/rpc-server-ts/commits/052325c4ae13e87f0b0e6db4fef45f248c9efa00))
* **TelemetryServer:** remove /telemetry route from httpserver ([95800eb](http://bitbucket.org/thermsio/rpc-server-ts/commits/95800eb3a3320f7fd0e41ab72e308a2c7316ee74))

## [1.12.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.12.2...v1.12.3) (2021-05-05)


### Bug Fixes

* tess ([2b22d7c](http://bitbucket.org/thermsio/rpc-server-ts/commits/2b22d7c907c1e9003c42d20ebc3d7c589bb7feee))
* **HeartbeatModel:** expire on createdAt prop isoString call ([d8c46f2](http://bitbucket.org/thermsio/rpc-server-ts/commits/d8c46f2995c478fc018f38aa85c0218528c666bf))
* **TelemetryServer:** move default to static file in http server when route not recognized ([9181eef](http://bitbucket.org/thermsio/rpc-server-ts/commits/9181eef086ad6625141c0dd159ffdc64a5258e46))

## [1.12.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.12.1...v1.12.2) (2021-05-05)


### Bug Fixes

* **TelemetryServer:** move default to static file in http server when route not recognized ([f883403](http://bitbucket.org/thermsio/rpc-server-ts/commits/f883403daa67258ae2fae8b09a6ff4b7062d4a77))

## [1.12.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.12.0...v1.12.1) (2021-05-05)


### Bug Fixes

* **HeartbeatModel:** expire on createdAt prop ([9358851](http://bitbucket.org/thermsio/rpc-server-ts/commits/93588516814540da91181763fbd5c52d7f5a8aa2))

# [1.12.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.11.4...v1.12.0) (2021-05-05)


### Features

* **TelemetryServer:** fallback url to serve index.html static file if static file path is set ([6341f54](http://bitbucket.org/thermsio/rpc-server-ts/commits/6341f54ab6e7b50cc64e01135321e9235bc8d5dd))

## [1.11.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.11.3...v1.11.4) (2021-05-05)


### Bug Fixes

* **TelemetryPersistence:** getHeartbeats query distinct with uniqueBy on ephemeralId ([bdbc9c2](http://bitbucket.org/thermsio/rpc-server-ts/commits/bdbc9c28780100e58f89a9a2f0d86291de61abd0))

## [1.11.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.11.2...v1.11.3) (2021-05-05)


### Bug Fixes

* **TelemetryPersistence:** getHeartbeats query distinct with uniqueBy on ephemeralId ([7d1862e](http://bitbucket.org/thermsio/rpc-server-ts/commits/7d1862e1e0b65a32973a71049d6c69cdcb9ef197))

## [1.11.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.11.1...v1.11.2) (2021-05-05)


### Bug Fixes

* **TelemetryPersistence:** distinct query heartbeats ([0502dd0](http://bitbucket.org/thermsio/rpc-server-ts/commits/0502dd0444ed5576fc2092c73adde60607838000))
* **TelemetryPersistence:** getHeartbeats query distinct on ephemeralId ([e33dfbf](http://bitbucket.org/thermsio/rpc-server-ts/commits/e33dfbf95b862ebb534810f9b27b417652ef0fa0))

## [1.11.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.11.0...v1.11.1) (2021-05-04)


### Bug Fixes

* heartbeat for telemetry ([5ad0c9d](http://bitbucket.org/thermsio/rpc-server-ts/commits/5ad0c9d0ed02a34c34c84579bc1739bf1cba5a6d))

# [1.11.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.10.1...v1.11.0) (2021-05-04)


### Bug Fixes

* **RPCServer:** tests ([66b07a6](http://bitbucket.org/thermsio/rpc-server-ts/commits/66b07a699814d57332b69178ae4d07fff246a22a))
* tests for mock mongo ([43c6cfb](http://bitbucket.org/thermsio/rpc-server-ts/commits/43c6cfb4b44906c220fdbff73d69a0f195dfbf9b))


### Features

* telemetry persistence and telemetry standalone server ([192d15c](http://bitbucket.org/thermsio/rpc-server-ts/commits/192d15c052a312ed7df79930059307db6dba2ad7))

## [1.10.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.10.0...v1.10.1) (2021-05-04)


### Bug Fixes

* **RPCServer:** #call returns and throws a CallResponse instance ([47afb27](http://bitbucket.org/thermsio/rpc-server-ts/commits/47afb27fe0956136efb7e3a186374d4e0410aab0))

# [1.10.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.9.1...v1.10.0) (2021-05-04)


### Features

* **RPCServer:** add RPCServer.call and unit tests ([54c3fb5](http://bitbucket.org/thermsio/rpc-server-ts/commits/54c3fb57553a8c2d77e19bac8d2b295a060e05e5))

## [1.9.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.9.0...v1.9.1) (2021-05-03)


### Bug Fixes

* **RPCServer:** registerHandler return type ([73655a3](http://bitbucket.org/thermsio/rpc-server-ts/commits/73655a3d79cf63867924cfc7ebcf1f6f938b8b1d))

# [1.9.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.8.0...v1.9.0) (2021-05-03)


### Features

* **RPCServer:** RPCServer.registerHandler now accepts a handler that returns a POJO ([42ebc63](http://bitbucket.org/thermsio/rpc-server-ts/commits/42ebc637ff2d0e35f3abf07348cc24dd407912a1))

# [1.8.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.7.4...v1.8.0) (2021-05-03)


### Features

* **RPCServer:** add #call public member to RPCServer for making requests ([6aaac5c](http://bitbucket.org/thermsio/rpc-server-ts/commits/6aaac5c44de23be2d805abf616034a4857546fda))

## [1.7.4](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.7.3...v1.7.4) (2021-04-30)


### Bug Fixes

* **LocalHandler:** trycatch on ajv json-schema compilation ([cfde8ba](http://bitbucket.org/thermsio/rpc-server-ts/commits/cfde8badffd04a0bd74550111fc1bbc2e5904e28))

## [1.7.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.7.2...v1.7.3) (2021-04-28)


### Bug Fixes

* **WebSocket:** allow requestDTO.identity to overwrite the in-memory identity for a connection ([d258fa6](http://bitbucket.org/thermsio/rpc-server-ts/commits/d258fa6a6d8e286329976cd032094452cfdef900))

## [1.7.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.7.1...v1.7.2) (2021-04-27)


### Bug Fixes

* **WebSocketServer:** update tests for fix to setting identity over WS ([fc690d7](http://bitbucket.org/thermsio/rpc-server-ts/commits/fc690d76c03cad87526db1e824303ca502aa6821))
* ts type ([49d2bf1](http://bitbucket.org/thermsio/rpc-server-ts/commits/49d2bf1ba0d89e4f94fcc5bd32e817093cb38593))
* **WebSocketServer:** send ws response after setting identity ([49c5745](http://bitbucket.org/thermsio/rpc-server-ts/commits/49c5745ceeed3fe35fca84a6982ed1b00d2afe77))

## [1.7.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.7.0...v1.7.1) (2021-04-27)


### Bug Fixes

* **WebSocketServer:** send ws response after setting identity ([57a554b](http://bitbucket.org/thermsio/rpc-server-ts/commits/57a554b47ce162284bb5fab73e0be01a253c86db))

# [1.7.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.6.3...v1.7.0) (2021-04-26)


### Features

* **WebSocketServer:** set and retain identity info for client connections ([e44d66e](http://bitbucket.org/thermsio/rpc-server-ts/commits/e44d66e7c552ad8e83942fd18413c141eb54ee43))

## [1.6.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.6.2...v1.6.3) (2021-04-25)


### Bug Fixes

* **LocalHandler:** catch throws in handler when they are async funcs ([96df32e](http://bitbucket.org/thermsio/rpc-server-ts/commits/96df32e1d6c1976fdbacb66f2ce1d48cf1c0b95c))

## [1.6.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.6.1...v1.6.2) (2021-04-25)


### Bug Fixes

* add debug logs to LocalHandler#manageRequest ([4882d0e](http://bitbucket.org/thermsio/rpc-server-ts/commits/4882d0e7c3a11cf342299fc9f379d3eae74adef5))

## [1.6.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.6.0...v1.6.1) (2021-04-25)


### Bug Fixes

* call handler request validation in LocalHandlerManager before calling actual handler ([8e5e557](http://bitbucket.org/thermsio/rpc-server-ts/commits/8e5e5576ff9f1f37aed50c0018448c10153fb090))

# [1.6.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.5.1...v1.6.0) (2021-04-25)


### Features

* add AJV validation to HandlerRegistrationInfo args ([06e35eb](http://bitbucket.org/thermsio/rpc-server-ts/commits/06e35eb7df8b58f8adaa8c7821a69bba38589c98))

## [1.5.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.5.0...v1.5.1) (2021-04-25)


### Bug Fixes

* handler args data types for registration docs ([d2d9f17](http://bitbucket.org/thermsio/rpc-server-ts/commits/d2d9f1741201be27b889e55f7087779df9660dad))

# [1.5.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.4.2...v1.5.0) (2021-04-25)


### Bug Fixes

* tests ([84ac691](http://bitbucket.org/thermsio/rpc-server-ts/commits/84ac691bf81a3338ce97e00a8805f75b5ca71634))


### Features

* catch handler and return 500 response ([3e36616](http://bitbucket.org/thermsio/rpc-server-ts/commits/3e36616157f7fb826a23271d8fe430bb1bb86ac3))

## [1.4.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.4.1...v1.4.2) (2021-04-19)


### Bug Fixes

* handler reg info optional props ([9f8508b](http://bitbucket.org/thermsio/rpc-server-ts/commits/9f8508b124854422000f0f22f3b0093c2a440e32))

## [1.4.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.4.0...v1.4.1) (2021-04-19)


### Bug Fixes

* export types remove .d.ts files ([9601fb5](http://bitbucket.org/thermsio/rpc-server-ts/commits/9601fb565e12e8bdd50c2c61d464e02163572b3c))

# [1.4.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.3.3...v1.4.0) (2021-04-03)


### Features

* add health endpoints for http server ([59a122e](http://bitbucket.org/thermsio/rpc-server-ts/commits/59a122ec9236dc1f898555275e7bf690ff8ac351))

## [1.3.3](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.3.2...v1.3.3) (2021-04-03)


### Bug Fixes

* manual fix bump ([0e29a59](http://bitbucket.org/thermsio/rpc-server-ts/commits/0e29a59e4c58c43572415239521308530ee5dda8))

## [1.3.2](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.3.1...v1.3.2) (2021-04-02)


### Bug Fixes

* build output ([859eae6](http://bitbucket.org/thermsio/rpc-server-ts/commits/859eae60a67e6cf35cb1088ddf2f9ab6eb4e67a2))

## [1.3.1](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.3.0...v1.3.1) (2021-04-02)


### Bug Fixes

* docs for websocket ([e6ffae0](http://bitbucket.org/thermsio/rpc-server-ts/commits/e6ffae03a71e84e734fe30d4a4430df060ef0f71))
* rabbit tests ([3b6b5be](http://bitbucket.org/thermsio/rpc-server-ts/commits/3b6b5beb7a2fe9a2d6eb80914d6ab5213ceff629))

# [1.3.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.2.0...v1.3.0) (2021-04-01)


### Bug Fixes

* HttpServer cors ([6fdaec6](http://bitbucket.org/thermsio/rpc-server-ts/commits/6fdaec6cb61dc1340cf0e46f005d9f9376512b91))
* remove unused types ([e52a93a](http://bitbucket.org/thermsio/rpc-server-ts/commits/e52a93ac65075658f64670719589d125229cf623))


### Features

* ws server implementation and tests ([5c0d2c9](http://bitbucket.org/thermsio/rpc-server-ts/commits/5c0d2c9278f27c996f24716a22788e2fb4921ef0))

# [1.2.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.1.0...v1.2.0) (2021-03-27)


### Features

* **HttpServer:** example setup for serving telemetry client file ([044597d](http://bitbucket.org/thermsio/rpc-server-ts/commits/044597dd16954a793fdbda935eec051f06dc00dd))

# [1.1.0](http://bitbucket.org/thermsio/rpc-server-ts/compare/v1.0.0...v1.1.0) (2021-03-27)


### Bug Fixes

* tests ([2646c7b](http://bitbucket.org/thermsio/rpc-server-ts/commits/2646c7beeed984800d31378290444bdfbd981d7e))
* **RabbitManager:** tests ([7ad03d1](http://bitbucket.org/thermsio/rpc-server-ts/commits/7ad03d1bc0a9fbde3f1ee7aef76eb7b29205d260))
* build script ([0552a59](http://bitbucket.org/thermsio/rpc-server-ts/commits/0552a5955aabbcb7637c9dc7862b0f3e1eb6a5a2))
* npmignore ([850cbc8](http://bitbucket.org/thermsio/rpc-server-ts/commits/850cbc88af749a587688c4e74c6158ed54de8ebc))


### Features

* telemetry, logging, msg broker WIP ([dfc0db1](http://bitbucket.org/thermsio/rpc-server-ts/commits/dfc0db188a8e1ff6052a227982c133af5a5b751e))

# 1.0.0 (2021-03-22)


### Features

* initial release ([22acddc](http://bitbucket.org/thermsio/rpc-server-ts/commits/22acddce8ab3d51852a1694d3418da0862771c6f))
