'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var ObjectId = require('bson-objectid');
var Ajv = require('ajv');
var Debug = require('debug');
var amqp = require('amqp-connection-manager');
var Http = require('http');
var Koa = require('koa');
var cors = require('@koa/cors');
var koaBodyParser = require('koa-bodyparser');
var WsServer = require('ws');
var uuid = require('node-uuid');
var os = require('os');
var lodash = require('lodash');
var mongoose = require('mongoose');
var logger = require('koa-logger');
var koaStatic = require('koa-static');
var koaSendFile = require('koa-sendfile');
var path = require('path');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var ObjectId__default = /*#__PURE__*/_interopDefaultLegacy(ObjectId);
var Ajv__default = /*#__PURE__*/_interopDefaultLegacy(Ajv);
var Debug__default = /*#__PURE__*/_interopDefaultLegacy(Debug);
var amqp__default = /*#__PURE__*/_interopDefaultLegacy(amqp);
var Http__default = /*#__PURE__*/_interopDefaultLegacy(Http);
var Koa__default = /*#__PURE__*/_interopDefaultLegacy(Koa);
var cors__default = /*#__PURE__*/_interopDefaultLegacy(cors);
var koaBodyParser__default = /*#__PURE__*/_interopDefaultLegacy(koaBodyParser);
var WsServer__default = /*#__PURE__*/_interopDefaultLegacy(WsServer);
var uuid__default = /*#__PURE__*/_interopDefaultLegacy(uuid);
var os__default = /*#__PURE__*/_interopDefaultLegacy(os);
var mongoose__default = /*#__PURE__*/_interopDefaultLegacy(mongoose);
var logger__default = /*#__PURE__*/_interopDefaultLegacy(logger);
var koaStatic__default = /*#__PURE__*/_interopDefaultLegacy(koaStatic);
var koaSendFile__default = /*#__PURE__*/_interopDefaultLegacy(koaSendFile);
var path__default = /*#__PURE__*/_interopDefaultLegacy(path);

const DEFAULT_REQUEST_SCOPE = 'global';
const DEFAULT_REQUEST_VERSION = '1';
class CallRequest {
    static fromCallRequestDTO = (callRequestDTO, details) => {
        if (!callRequestDTO.procedure) {
            throw new Error('procedure is required');
        }
        else if (!details?.trace?.caller) {
            throw new Error('trace.caller is required');
        }
        return new CallRequest({
            args: callRequestDTO.args,
            correlationId: callRequestDTO.correlationId,
            identity: callRequestDTO.identity,
            procedure: callRequestDTO.procedure,
            scope: callRequestDTO.scope,
            version: callRequestDTO.version,
            trace: {
                caller: details.trace.caller,
                id: callRequestDTO.correlationId || new ObjectId__default["default"]().toString(),
                internal: !!details.trace.internal,
                ipAddress: details.trace.ipAddress,
            },
        });
    };
    static EMPTY = new CallRequest({
        procedure: 'empty',
        trace: {
            caller: 'empty',
            id: '0',
        },
    });
    static isCallRequest(obj) {
        if (typeof obj === 'object') {
            return (obj.hasOwnProperty('procedure') &&
                obj.hasOwnProperty('scope') &&
                obj.hasOwnProperty('version') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    static toCallRequestDescription = (request) => {
        return {
            procedure: request.procedure,
            scope: request.scope || DEFAULT_REQUEST_SCOPE,
            version: request.version || DEFAULT_REQUEST_VERSION,
        };
    };
    constructor(request) {
        this.args = request.args;
        this.correlationId = request.correlationId;
        this.identity = request.identity;
        this.procedure = request.procedure;
        this.scope = request.scope || DEFAULT_REQUEST_SCOPE;
        this.version = request.version || DEFAULT_REQUEST_VERSION;
        this.trace = request.trace;
    }
    args;
    correlationId;
    identity;
    procedure;
    scope;
    version;
    trace;
}

class CallResponse {
    static EMPTY = new CallResponse({
        code: 500,
        message: 'empty',
        success: false,
    }, CallRequest.EMPTY);
    static isCallResponse(obj) {
        if (typeof obj === 'object') {
            return (obj.hasOwnProperty('code') &&
                obj.hasOwnProperty('success') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    static isCallResponseDTO(obj) {
        if (typeof obj === 'object') {
            return obj.hasOwnProperty('code') && obj.hasOwnProperty('success');
        }
        return false;
    }
    static toCallResponseDTO = (response) => {
        return {
            code: response.code,
            correlationId: response.correlationId,
            data: response.data,
            message: response.message,
            success: response.success,
        };
    };
    constructor(response, request) {
        this.code = response.code;
        this.correlationId = request.correlationId || response.correlationId;
        this.data = response.data;
        this.message = response.message;
        this.success = response.success;
        this.trace = {
            id: request.trace.id,
        };
    }
    code;
    correlationId;
    data;
    message;
    success;
    trace;
}

function makeHandlerRequestKey(request) {
    return `${request.scope}::${request.procedure}::${request.version}`;
}

const RPCEventTopics = {
    handler_call_success: 'handler_call_success',
    handler_error: 'handler_error',
    rpc_server_error: 'rpc_server_error',
};

const ajv = new Ajv__default["default"]({
    coerceTypes: 'array',
    removeAdditional: true,
    useDefaults: true,
});
const debug$6 = Debug__default["default"]('rpc:LocalHandlerManager');
class LocalHandlerManager {
    options;
    eventBus;
    constructor(options) {
        this.options = options;
        this.eventBus = options.eventBus;
    }
    handlersByHandlerRequestKey = {};
    handlerRequestValidationsByHandlerRequestKey = {};
    checkCanHandleRequest = (request) => {
        return !!this.handlersByHandlerRequestKey[makeHandlerRequestKey(request)];
    };
    getRegisteredHandlers = () => {
        return Object.keys(this.handlersByHandlerRequestKey);
    };
    getSimilarRegisteredProcedures = (request) => {
        const rgx = new RegExp(request.procedure);
        return Object.keys(this.handlersByHandlerRequestKey)
            .filter((key) => {
            return rgx.test(key);
        })
            .join(', ');
    };
    manageRequest = async (request) => {
        debug$6('manageRequest', request);
        let onHandlerStartRequestReturnedValue;
        if (this.options.onHandlerStartRequest) {
            onHandlerStartRequestReturnedValue =
                this.options.onHandlerStartRequest(request);
        }
        try {
            const key = makeHandlerRequestKey(CallRequest.toCallRequestDescription(request));
            if (this.handlersByHandlerRequestKey[key].internalOnly &&
                !request.trace.internal) {
                debug$6('handler is marked "internalOnly" and the request is not coming from internal', request);
                throw new CallResponse({
                    code: 403,
                    message: 'caller is not authorized to access this procedure',
                    success: false,
                }, request);
            }
            if (this.handlerRequestValidationsByHandlerRequestKey[key]) {
                this.handlerRequestValidationsByHandlerRequestKey[key](request);
            }
            return await this.handlersByHandlerRequestKey[key].handler(request);
        }
        catch (e) {
            if (e instanceof CallResponse) {
                debug$6('manageRequest catching a thrown CallResponse');
                return e;
            }
            this.eventBus.publish({
                payload: {
                    error: e,
                    request,
                },
                topic: RPCEventTopics.handler_error,
            });
            console.log('LocalHandlerManager error: catching and returning 500', e);
            return new CallResponse({
                code: 500,
                success: false,
            }, request);
        }
        finally {
            if (this.options.onHandlerEndRequest) {
                this.options.onHandlerEndRequest(request, onHandlerStartRequestReturnedValue);
            }
        }
    };
    registerHandler = (request, handler) => {
        debug$6('registerHandler', request);
        const key = makeHandlerRequestKey(request);
        this.handlersByHandlerRequestKey[key] = {
            internalOnly: !!request.internal,
            handler,
        };
        if (typeof request.args === 'object' && request.args?.type) {
            try {
                const validate = ajv.compile(request.args);
                this.handlerRequestValidationsByHandlerRequestKey[key] = (request) => {
                    const valid = validate(request.args || {});
                    if (!valid) {
                        debug$6('validation error', validate.errors);
                        throw new CallResponse({
                            code: 422,
                            message: `"args" validation failed: ${validate.errors
                                ?.map((errorObj) => errorObj.instancePath + errorObj.message)
                                .join(', ')}`,
                            success: false,
                        }, request);
                    }
                    return undefined;
                };
            }
            catch (e) {
                console.log(``);
                console.log(`ERROR: Unable to compile JSON-schema for "${key}"`);
                console.log(`   ${e.message}`);
                console.log(``);
                process.exit(1);
            }
        }
    };
}

const DEFAULT_TIMEOUT = 30000;
class PromiseTimeoutError extends Error {
}
class PromiseWrapper {
    rejectPromise;
    resolvePromise;
    promise;
    constructor(timeout) {
        this.promise = new Promise((resolve, reject) => {
            const timer = setTimeout(() => {
                reject(new PromiseTimeoutError('PromiseWraper timeout'));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        });
    }
    reject = (rejectReturnValue) => {
        if (this.rejectPromise) {
            this.rejectPromise(rejectReturnValue);
        }
    };
    resolve = (resolveReturnValue) => {
        if (this.resolvePromise) {
            this.resolvePromise(resolveReturnValue);
        }
    };
}

const RPC_GLOBAL_EXCHANGE_NAME = 'rpc-global';
const RPC_SCOPED_EXCHANGE_NAME = 'rpc-scoped';
const debug$5 = Debug__default["default"]('rpc:RabbitRpc');
var QueueMessageTypes;
(function (QueueMessageTypes) {
    QueueMessageTypes["Request"] = "request";
    QueueMessageTypes["Response"] = "response";
})(QueueMessageTypes || (QueueMessageTypes = {}));
class RabbitRpc {
    options;
    channelWrapper;
    eventBus;
    pendingPromisesForResponse = {};
    requestHandler;
    uniqueQueueName;
    constructor(options) {
        this.options = options;
        this.eventBus = options.eventBus;
        this.uniqueQueueName = `${options.config.displayName}-rpc-${new ObjectId__default["default"]().toString()}`;
        this.requestHandler = options.requestHandler;
        this.channelWrapper = options.connection.createChannel({
            json: true,
            setup: (channel) => {
                channel.addListener('return', this.onReturnedQueueMsg);
                return Promise.all([
                    channel.assertExchange(RPC_GLOBAL_EXCHANGE_NAME, 'direct'),
                    channel.assertExchange(RPC_SCOPED_EXCHANGE_NAME, 'direct'),
                    channel.assertQueue(this.uniqueQueueName, {
                        autoDelete: true,
                        durable: false,
                    }),
                    channel.consume(this.uniqueQueueName, this.onMsgReceivedFromQueue, {
                        noAck: true,
                    }),
                ]);
            },
        });
    }
    sendRequestForResponse = async (request) => {
        debug$5('sendRequestForResponse()');
        const promiseWrapper = new PromiseWrapper();
        this.pendingPromisesForResponse[request.trace.id] = promiseWrapper;
        const exchange = request.scope === DEFAULT_REQUEST_SCOPE
            ? RPC_GLOBAL_EXCHANGE_NAME
            : RPC_SCOPED_EXCHANGE_NAME;
        await this.channelWrapper.publish(exchange, this.makeExchangeRoutingKey(CallRequest.toCallRequestDescription(request)), this.makeQueueRPCObject({ request }), {
            appId: this.options.config.displayName,
            correlationId: request?.trace?.id,
            mandatory: true,
            replyTo: this.uniqueQueueName,
            type: QueueMessageTypes.Request,
        });
        try {
            return await promiseWrapper.promise;
        }
        catch (e) {
            let message = '';
            if (e instanceof PromiseTimeoutError) {
                message = 'no response received (timed out)';
            }
            else {
                message = 'there was an error processing RPC';
            }
            this.eventBus.publish({
                payload: {
                    error: e,
                    request,
                },
                topic: RPCEventTopics.rpc_server_error,
            });
            return new CallResponse({
                code: 500,
                message,
                success: false,
            }, request);
        }
    };
    shutdown = () => this.channelWrapper.close();
    subscribeToHandleRequest = async (request) => {
        debug$5('subscribeToHandleRequest', request);
        if (!this.channelWrapper) {
            throw new Error('cannot subscribe request handler because rabbitmq was not initialized properly');
        }
        this.channelWrapper?.addSetup(async (channel) => {
            const exchange = request.scope === DEFAULT_REQUEST_SCOPE
                ? RPC_GLOBAL_EXCHANGE_NAME
                : RPC_SCOPED_EXCHANGE_NAME;
            await channel.bindQueue(this.uniqueQueueName, exchange, this.makeExchangeRoutingKey(request));
        });
    };
    error = (...args) => console.log('RabbitRpc ERROR', ...args);
    handleReturnedQueueMsg = async (msg) => {
        debug$5('handleReturnedQueueMsg()');
        const { request } = this.parseQueueMsgContent(msg);
        if (!request) {
            this.error('rcvd returned queue msg and cannot handle', msg);
            return;
        }
        if (this.pendingPromisesForResponse[request.trace.id]) {
            this.pendingPromisesForResponse[request.trace.id].resolve(new CallResponse({
                code: 501,
                message: 'no registered handlers for RPC',
                success: false,
            }, request));
        }
        else {
            debug$5('a queue message was returned but no pending requests found', request, msg);
        }
    };
    handleRequestFromQueue = async (msg) => {
        debug$5('handleRequestFromQueue()');
        const { properties } = msg;
        const { request } = this.parseQueueMsgContent(msg);
        if (!request) {
            this.error('rcvd queue msg that is not a valid RPC request', msg);
            return;
        }
        let response;
        try {
            response = await this.requestHandler(request);
            if (!response) {
                throw new Error('request handler provided no response');
            }
        }
        catch (e) {
            this.eventBus.publish({
                payload: {
                    error: e,
                    request,
                    response: undefined,
                },
                topic: RPCEventTopics.rpc_server_error,
            });
            this.error('error handling request from the queue', e);
            this.error('   request: ', request);
        }
        try {
            if (response) {
                await this.sendHandledResponseToQueue(properties.replyTo, response, request);
            }
        }
        catch (e) {
            this.eventBus.publish({
                payload: {
                    error: e,
                    request,
                    response: undefined,
                },
                topic: RPCEventTopics.rpc_server_error,
            });
            this.error('error sending response to the queue', e);
            this.error('   response: ', response);
        }
    };
    handleResponseFromQueue = async (msg) => {
        debug$5('handleResponseFromQueue()');
        const { response, request } = this.parseQueueMsgContent(msg);
        if (!request || !response) {
            this.error('rcvd queue msg that is not a valid RPC response', msg);
            return;
        }
        if (this.pendingPromisesForResponse[response.trace.id]) {
            this.pendingPromisesForResponse[response.trace.id].resolve(response);
        }
        else {
            this.error('received queue msg response with no pending request found locally', request, response);
        }
    };
    makeExchangeRoutingKey = (request) => makeHandlerRequestKey(request);
    makeQueueRPCObject = ({ request, response, }) => ({
        request,
        response,
    });
    onReturnedQueueMsg = (msg) => {
        debug$5('onReturnedQueueMsg()');
        this.handleReturnedQueueMsg(msg);
    };
    onMsgReceivedFromQueue = async (msg) => {
        debug$5('handleMsgFromQueue() message', msg);
        if (!msg) {
            debug$5('queue consumer received empty msg');
            return;
        }
        switch (msg.properties.type) {
            case QueueMessageTypes.Response:
                this.handleResponseFromQueue(msg);
                break;
            case QueueMessageTypes.Request:
                this.handleRequestFromQueue(msg);
                break;
            default:
                this.error('rcvd rabbit queue msg with unrecognized "type" property: ', msg);
        }
    };
    parseQueueMsgContent = (msg) => {
        try {
            let json;
            json = JSON.parse(msg.content.toString());
            if (!json.request) {
                debug$5('parseQueueMsg() json has no "request" prop', json);
                return {};
            }
            const response = json.response
                ? new CallResponse(json.response, json.request)
                : undefined;
            const request = new CallRequest(json.request);
            return {
                response,
                request,
            };
        }
        catch (e) {
            this.eventBus.publish({
                payload: {
                    error: e,
                },
                topic: RPCEventTopics.rpc_server_error,
            });
            this.error('unable to parse queue msg content', e, msg);
            return {};
        }
    };
    sendHandledResponseToQueue = async (queueName, response, request) => {
        debug$5('sendHandledResponseToQueue() sending response for a request', request);
        await this.channelWrapper.sendToQueue(queueName, this.makeQueueRPCObject({ request, response }), {
            appId: this.options.config.displayName,
            correlationId: request?.trace?.id,
            type: QueueMessageTypes.Response,
        });
    };
}

const debug$4 = Debug__default["default"]('rpc:RabbitManager');
const error = (...args) => console.log('RabbitManager Err:', ...args);
class RabbitManager {
    options;
    connected = false;
    connection;
    rpc;
    uniqueQueueName;
    constructor(options) {
        this.options = options;
        options.requestHandler;
        this.uniqueQueueName = `${options.config.displayName}-${options.config.ephemeralId}`;
        if (!options.config?.messageBroker?.amqpURI) {
            throw new Error('amqpURI is required');
        }
        this.connection = amqp__default["default"].connect([options.config.messageBroker.amqpURI], {
            heartbeatIntervalInSeconds: 5,
        });
        this.connection.on('connect', ({ url }) => {
            this.connected = true;
            debug$4('rabbit connected to: ', url);
        });
        this.connection.on('disconnect', ({ err }) => {
            if (this.connected) {
                error('disconnected from rabbit broker');
            }
            else {
                error('failed to connect to rabbit broker');
            }
        });
        this.rpc = new RabbitRpc({
            config: options.config,
            connection: this.connection,
            eventBus: options.eventBus,
            requestHandler: options.requestHandler,
        });
    }
    sendRequestForResponse = (request) => {
        return this.rpc.sendRequestForResponse(request);
    };
    shutdown = async () => {
        try {
            await this.rpc.shutdown();
            await this.connection.close();
        }
        finally {
        }
    };
    subscribeToHandleRequest = (request) => {
        return this.rpc.subscribeToHandleRequest(request);
    };
}

class MessageBrokerManager {
    options;
    messageQueue;
    constructor(options) {
        this.options = options;
        if (!options.config.messageBroker?.amqpURI)
            throw new Error('cannot be initialized without config.messageBroker.amqpURI connection info for a MessageBroker');
        this.messageQueue = new RabbitManager(options);
    }
    manageRequest = async (request) => {
        return this.messageQueue.sendRequestForResponse(request);
    };
    registerHandler = async (request) => {
        return this.messageQueue.subscribeToHandleRequest(request);
    };
}

const debug$3 = Debug__default["default"]('rpc:CallManager');
class CallManager {
    options;
    localHandlerManager;
    messageBrokerManager;
    constructor(options) {
        this.options = options;
        this.localHandlerManager = new LocalHandlerManager({
            onHandlerEndRequest: options.config.handlers?.onHandlerEndRequest,
            onHandlerStartRequest: options.config.handlers?.onHandlerStartRequest,
            eventBus: options.eventBus,
        });
        if (options.config?.messageBroker) {
            this.messageBrokerManager = new MessageBrokerManager({
                config: options.config,
                eventBus: options.eventBus,
                requestHandler: this.manageRequest,
            });
        }
    }
    getRegisteredHandlers = () => this.localHandlerManager.getRegisteredHandlers();
    manageRequest = async (request) => {
        const localHandler = this.localHandlerManager.checkCanHandleRequest(CallRequest.toCallRequestDescription(request));
        debug$3('manageRequest', request);
        if (localHandler) {
            debug$3('manageRequest, local handler found');
            return this.localHandlerManager.manageRequest(request);
        }
        if (this.messageBrokerManager) {
            debug$3('manageRequest, sending to MessageBrokerManager');
            return this.messageBrokerManager.manageRequest(request);
        }
        else {
            debug$3(`no messageBrokerManager found to proxy request, returning 501 no handler found (not implemented) for ${makeHandlerRequestKey(request)}`);
            return new CallResponse({
                code: 501,
                message: `No procedure handler found for scope=${request.scope}, procedure=${request.procedure}, version=${request.version}. Trace info: caller=${request.trace.caller} internal=${!!request
                    .trace.internal}`,
                success: false,
            }, request);
        }
    };
    registerHandler = (request, handler) => {
        this.localHandlerManager.registerHandler(request, handler);
        if (this.messageBrokerManager) {
            this.messageBrokerManager.registerHandler(request);
        }
    };
}

const DEFAULT_BIND$1 = '127.0.0.1';

const debug$2 = Debug__default["default"]('rpc:HttpServer');
class HttpServer$1 {
    options;
    eventBus;
    httpServer;
    koaApp;
    serverListening = false;
    requestHandler;
    constructor(options) {
        this.options = options;
        this.eventBus = options.eventBus;
        this.koaApp = new Koa__default["default"]({ proxy: true });
        this.koaApp.use(async (ctx, next) => {
            try {
                await next();
            }
            catch (err) {
                console.log('There was an error in the Koa app from an incoming request', err);
                ctx.body = {
                    code: 400,
                    message: 'unable to parse & handle request',
                    success: false,
                };
            }
        });
        this.koaApp.use(koaBodyParser__default["default"]());
        this.koaApp.use(cors__default["default"]({ maxAge: 600, origin: '*' }));
        this.httpServer = Http__default["default"].createServer(this.koaApp.callback());
        this.setup();
    }
    name = 'HttpServer';
    static parseIPAddressFromHttpReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
    setIncomingRequestHandler = (requestHandler) => {
        this.requestHandler = requestHandler;
    };
    start = (onStartedCallback) => {
        if (!this.requestHandler) {
            throw new Error(`No request handler has be set`);
        }
        if (this.serverListening) {
            debug$2('cannot start again, already runnning');
            return;
        }
        const host = this.options.config.gatewayServer?.http?.bind || DEFAULT_BIND$1;
        const port = this.options.config.gatewayServer?.http?.port;
        this.httpServer.listen({
            host,
            port,
        }, () => {
            debug$2(`${this.options.config.displayName} listening ${host}:${port}`);
            this.serverListening = true;
            if (onStartedCallback) {
                onStartedCallback();
            }
        });
    };
    stop = (onStoppedCallback) => {
        const host = this.options.config.gatewayServer?.http?.bind || DEFAULT_BIND$1;
        const port = this.options.config.gatewayServer?.http?.port;
        this.httpServer?.close((err) => {
            if (err) {
                debug$2(`error when stopping (${host}:${port})`, err);
            }
            else {
                debug$2(`stopped (${host}:${port})`);
                this.serverListening = false;
            }
            if (onStoppedCallback) {
                onStoppedCallback();
            }
        });
    };
    setup = () => {
        this.koaApp.use(async (ctx) => {
            debug$2('received request');
            if (ctx.request.path === '/health') {
                debug$2('returning health check status');
                ctx.body = 'ok';
                ctx.status = 200;
                return;
            }
            if (!ctx.request.body ||
                typeof ctx.request.body !== 'object' ||
                !Object.keys(ctx.request.body).length) {
                debug$2('empty request body received', ctx.request.body);
                ctx.body = {
                    code: 400,
                    message: 'Empty request body received',
                    success: false,
                };
                return;
            }
            const requestDTO = ctx.request.body;
            let request;
            try {
                request = CallRequest.fromCallRequestDTO(requestDTO, {
                    trace: {
                        caller: 'HttpServer',
                        ipAddress: HttpServer$1.parseIPAddressFromHttpReq(ctx.req),
                    },
                });
                debug$2(`request "${makeHandlerRequestKey(request)}"`);
            }
            catch (e) {
                debug$2('error converting request body to CallRequest', e);
                ctx.body = {
                    code: 400,
                    correlationId: request?.correlationId,
                    message: 'unable to parse request body as a valid CallRequest',
                    success: false,
                };
                return;
            }
            try {
                debug$2('sending request to requestHandler');
                const response = await this.requestHandler(request);
                debug$2('returning request response');
                ctx.body = CallResponse.toCallResponseDTO(response);
            }
            catch (e) {
                this.eventBus.publish({
                    payload: {
                        error: e,
                        request,
                    },
                    topic: RPCEventTopics.rpc_server_error,
                });
                console.log('HttpSerer error: there was an error handling request:', e, request);
                ctx.body = {
                    code: 500,
                    correlationId: request?.correlationId,
                    message: 'There was a problem calling the procedure',
                    success: false,
                };
            }
            return;
        });
    };
}

function isIdentityValid(identity) {
    if (!identity)
        return false;
    if (identity.authorization && typeof identity.authorization !== 'string')
        return false;
    if (identity.deviceName && typeof identity.deviceName !== 'string')
        return false;
    if (identity.metadata && typeof identity.metadata !== 'object')
        return false;
    return true;
}

const debug$1 = Debug__default["default"]('rpc:WebSocketServer');
class WebSocketServer {
    options;
    bind;
    eventBus;
    port;
    requestHandler;
    wss;
    constructor(options) {
        this.options = options;
        if (!options.config.gatewayServer?.websocket?.port) {
            throw new Error('config.gatewayServer.websocket.port is required');
        }
        this.bind = options.config.gatewayServer?.websocket?.bind || DEFAULT_BIND$1;
        this.eventBus = options.eventBus;
        this.port = options.config.gatewayServer?.websocket?.port;
    }
    name = 'WebSocketServer';
    static parseIPAddressFromWsReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
    setIncomingRequestHandler = (requestHandler) => {
        this.requestHandler = requestHandler;
    };
    start = (onStartedCallback) => {
        if (!this.requestHandler) {
            throw new Error(`No request handler has be set`);
        }
        this.wss = new WsServer__default["default"].Server({
            host: this.bind,
            port: this.port,
        });
        this.wss.on('connection', (ws, req) => {
            const websocketId = uuid__default["default"].v4();
            debug$1('websocket connected, connectionId:', websocketId);
            let identity;
            const wsIp = WebSocketServer.parseIPAddressFromWsReq(req);
            if (this.options.config?.gatewayServer?.websocket?.onClientConnect) {
                this.options.config.gatewayServer.websocket.onClientConnect({
                    connectionId: websocketId,
                    identity,
                    ip: wsIp,
                });
            }
            ws.on('close', (code, data) => {
                const reason = data.toString();
                debug$1('websocket disconnected, connectionId:', websocketId, reason);
                if (this.options.config?.gatewayServer?.websocket?.onClientDisconnect) {
                    this.options.config.gatewayServer.websocket.onClientDisconnect({
                        connectionId: websocketId,
                        identity,
                        ip: wsIp,
                    });
                }
            });
            ws.on('message', async (data, isBinary) => {
                const message = isBinary ? data : data.toString();
                debug$1('received message from WS, connectionId:', websocketId, message);
                let messageParsed;
                if (typeof message === 'string') {
                    try {
                        messageParsed = JSON.parse(message);
                    }
                    catch (e) {
                        debug$1('WebSocketServer error: received unparseable WS msg string', message);
                        return;
                    }
                }
                else {
                    debug$1('WebSocketServer error: received unknown "message" from client', message);
                }
                const requestDTO = messageParsed;
                if (requestDTO.identity) {
                    if (!isIdentityValid(requestDTO.identity)) {
                        console.log('error recieving CallResponseDTO with invalid identity schema', requestDTO.identity);
                        return;
                    }
                    if (identity?.authorization &&
                        identity.authorization !== requestDTO.identity.authorization) {
                        console.log('error, a websocket changed identity.authorization from previous value, disconnecting...');
                        ws.send(JSON.stringify(CallResponse.toCallResponseDTO(new CallResponse({
                            code: 400,
                            message: 'identity.authorization has changed - disconnecting websocket connection',
                            success: false,
                        }, CallRequest.fromCallRequestDTO({
                            ...requestDTO,
                            procedure: '__internal-identity__',
                        }, { trace: { caller: 'WebSocketServer' } })))), () => ws.close());
                        return;
                    }
                    if (!identity?.authorization && requestDTO.identity?.authorization) {
                        if (this.options.config?.gatewayServer?.websocket?.onClientConnect) {
                            this.options.config.gatewayServer.websocket.onClientConnect({
                                connectionId: websocketId,
                                identity: requestDTO.identity,
                                ip: wsIp,
                            });
                        }
                    }
                    identity = requestDTO.identity;
                }
                if (!requestDTO.procedure && requestDTO.identity) {
                    try {
                        const response = new CallResponse({
                            code: 200,
                            correlationId: requestDTO.correlationId,
                            success: true,
                        }, CallRequest.fromCallRequestDTO({
                            ...requestDTO,
                            procedure: '__internal-set-identity__',
                        }, { trace: { caller: 'WebSocketServer' } }));
                        ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                    }
                    catch (e) {
                        console.log('error sending CallResponseDTO to WS connection after setting RPCClientIdentity, error:', e);
                    }
                    return;
                }
                let request;
                try {
                    request = CallRequest.fromCallRequestDTO({ identity, ...requestDTO }, { trace: { caller: 'WebSocketServer', ipAddress: wsIp } });
                }
                catch (e) {
                    debug$1('WebSocketServer error: WS msg not a valid CallRequestDTO', request, e);
                    return;
                }
                if (request) {
                    debug$1('handling received request');
                    if (request.identity) {
                        if (!isIdentityValid(request.identity)) {
                            console.log('error recieving CallResponseDTO with invalid RPCClientIdentity schema', request.identity);
                            return;
                        }
                        identity = request.identity;
                    }
                    const response = await this.handleIncomingRequest(request);
                    debug$1('returning request response');
                    try {
                        ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                    }
                    catch (e) {
                        console.log('WebSocketServer error: error sending CallResponseDTO to WS connection', response, e);
                        this.eventBus.publish({
                            payload: {
                                error: e,
                                request,
                                response,
                            },
                            topic: RPCEventTopics.rpc_server_error,
                        });
                    }
                    return;
                }
            });
        });
        this.wss.once('listening', () => {
            debug$1(`${this.options.config.displayName} listening ${this.bind}:${this.port}`);
            if (onStartedCallback) {
                onStartedCallback();
            }
        });
    };
    stop = (onStoppedCallback) => {
        this.wss?.close((err) => {
            if (err) {
                console.log('WSS error onclose:', err);
            }
            if (onStoppedCallback)
                onStoppedCallback();
        });
        for (const ws of this.wss?.clients ?? []) {
            ws.terminate();
        }
    };
    handleIncomingRequest = async (request) => {
        try {
            debug$1('sending request to requestHandler');
            const response = await this.requestHandler(request);
            return response;
        }
        catch (e) {
            if (e instanceof CallResponse) {
                debug$1('handleIncomingRequest catching a throw CallResponse');
                return e;
            }
            this.eventBus.publish({
                payload: {
                    error: e,
                    request,
                },
                topic: RPCEventTopics.rpc_server_error,
            });
            console.log('WebSocketServer error: there was an error handling request:', request, e);
            return new CallResponse({
                code: 500,
                message: 'There was a problem calling the procedure',
                success: false,
            }, request);
        }
    };
}

class EventBus {
    topics = {};
    constructor() { }
    publish = (event) => {
        if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
            return;
        }
        this.topics[event.topic].forEach(async (item) => {
            item(event.payload);
        });
    };
    publishAsync = (event) => {
        if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
            return Promise.resolve();
        }
        const promises = [];
        this.topics[event.topic].forEach((topicSubscriptionHandler) => {
            const returned = topicSubscriptionHandler(event.payload);
            if (returned && returned.then)
                promises.push(returned);
        });
        return Promise.all(promises).then(() => undefined);
    };
    publishSync = (event) => {
        if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
            return;
        }
        this.topics[event.topic].forEach((item) => {
            item(event.payload);
        });
    };
    subscribe = (topic, handler) => {
        if (!this.topics.hasOwnProperty.call(this.topics, topic)) {
            this.topics[topic] = [];
        }
        const index = this.topics[topic].push(handler) - 1;
        return {
            unsubscribe: () => {
                delete this.topics[topic][index];
            },
        };
    };
    unsubscribe = (event, handler) => {
        if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
            this.topics[event.topic] = [];
        }
        this.topics[event.topic].forEach((topicSubscriptionHandler, index) => {
            if (topicSubscriptionHandler === handler) {
                delete this.topics[event.topic][index];
            }
        });
    };
}

function deepFreeze(object) {
    const propNames = Object.getOwnPropertyNames(object);
    for (const name of propNames) {
        const value = object[name];
        if (value && typeof value === 'object') {
            deepFreeze(value);
        }
    }
    return Object.freeze(object);
}

const HeartbeatModel = mongoose__default["default"].model('Heartbeat', new mongoose__default["default"].Schema({
    createdAt: { default: Date.now, type: Date, expires: 60 },
    hostName: String,
    ephemeralId: String,
    gatewayHttpServer: Boolean,
    gatewayWebSocketServer: Boolean,
    displayName: String,
    startTime: String,
}));
const ProcedureDescriptionModel = mongoose__default["default"].model('ProcedureDescription', new mongoose__default["default"].Schema({
    args: mongoose.Schema.Types.Mixed,
    createdAt: String,
    data: mongoose.Schema.Types.Mixed,
    description: String,
    identity: [String],
    internal: Boolean,
    procedure: { required: true, type: String },
    scope: { default: 'global', type: String },
    serverDisplayName: String,
    version: { default: '1', type: String },
}));

class MongoManager {
    mongoURI;
    showDbConnectionLogs = false;
    connected = false;
    constructor(mongoURI) {
        this.mongoURI = mongoURI;
        this.showDbConnectionLogs = process.env.NODE_ENV !== 'test';
    }
    setupMongoConnection = async () => {
        mongoose__default["default"].connect(this.mongoURI, {
            autoCreate: true,
        });
        const db = mongoose__default["default"].connection;
        db.on('disconnected', (info) => {
            if (this.showDbConnectionLogs) {
                console.log('MongoManager disconnected:', info);
            }
            this.connected = false;
        });
        db.on('error', (err) => {
            console.error('MongoManager connection error:', err);
            this.connected = false;
        });
        db.once('connected', () => {
            if (this.showDbConnectionLogs) {
                console.log('MongoManager connected:');
            }
            this.connected = true;
        });
    };
    tearDownConnection = () => {
        mongoose__default["default"].disconnect();
    };
}

class TelemetryPersistenceManager {
    mongoManager;
    constructor(adaptorConfig) {
        this.mongoManager = new MongoManager(adaptorConfig.mongoURI);
        this.mongoManager.setupMongoConnection();
    }
    disconnect = () => {
        this.mongoManager.tearDownConnection();
    };
    getHeartbeats = async () => {
        return HeartbeatModel.find({}, null, { limit: 500 })
            .lean()
            .exec()
            .then((docs) => {
            return lodash.uniqBy(docs, 'ephemeralId');
        });
    };
    getProcedureDocs = async () => {
        return ProcedureDescriptionModel.find({}, null, { limit: 5000 })
            .lean()
            .exec();
    };
    saveHeartbeat = async (telemetryHeartbeat) => {
        const doc = new HeartbeatModel(telemetryHeartbeat);
        doc.save((err) => {
            if (err)
                return console.error('TelemetryPersistenceManager#saveHeartbeat error:', err);
        });
    };
    saveHandlersRegistrationInfo = async (procedureDescriptions) => {
        if (!procedureDescriptions.length)
            return;
        let waitCount = 0;
        while (!this.mongoManager.connected) {
            if (waitCount > 30) {
                throw Error('MongoManager has not connected to mongoDB');
            }
            waitCount++;
            await new Promise((r) => setTimeout(r, 100));
        }
        try {
            const serverDisplayName = procedureDescriptions[0].serverDisplayName;
            await ProcedureDescriptionModel.deleteMany({ serverDisplayName });
            await ProcedureDescriptionModel.insertMany(procedureDescriptions);
        }
        catch (err) {
            console.error('TelemetryPersistenceManager#saveHandlerRegistrationInfo error:', err);
        }
    };
    saveStatistics = async (telemetryStatistics) => { };
}

class TelemetryInfoManager {
    options;
    heartbeatData = {
        hostName: os__default["default"].hostname(),
        ephemeralId: '',
        gatewayHttpServer: false,
        gatewayWebSocketServer: false,
        displayName: '',
        startTime: '',
    };
    heartbeatInterval;
    registeredHandlers = [];
    statistics = {
        gateway: {
            http: {
                receivedCount: 0,
            },
            websocket: {
                receivedCount: 0,
            },
        },
        localHandlers: {
            errorCountByProcedure: {},
            errorCount: 0,
            handledCount: 0,
            handledCountByProcedure: {},
        },
        messageBroker: {
            receivedCount: 0,
            sentCount: 0,
        },
        handlerErrorCount: 0,
        handlerSuccessCount: 0,
        rpcServerErrorCount: 0,
    };
    telemetryPersistenceManager;
    constructor(options) {
        this.options = options;
        if (!options.config.telemetry?.adapter) {
            throw new Error('config.telemetry.adapter must be set');
        }
        this.telemetryPersistenceManager = new TelemetryPersistenceManager(options.config.telemetry?.adapter);
        this.heartbeatData.ephemeralId = options.config.ephemeralId;
        this.heartbeatData.gatewayHttpServer = !!options.config.gatewayServer?.http;
        this.heartbeatData.gatewayWebSocketServer = !!options.config.gatewayServer
            ?.websocket;
        this.heartbeatData.displayName = options.config.displayName;
        this.heartbeatData.startTime = new Date().toISOString();
        this.setupEventBusListeners();
    }
    getHeartbeatData = () => this.heartbeatData;
    getRegisteredHandlers = () => this.registeredHandlers;
    getStatistics = () => this.statistics;
    registerHandler = (request) => {
        this.registeredHandlers.push(request);
    };
    startTelemetryReporting = async () => {
        this.heartbeatInterval = setInterval(() => {
            this.saveHeartbeat();
        }, 30000);
        await this.saveHeartbeat();
        await this.telemetryPersistenceManager.saveHandlersRegistrationInfo(this.registeredHandlers.map((request) => ({
            ...request,
            createdAt: new Date().toISOString(),
            serverDisplayName: this.options.config.displayName,
        })));
    };
    stopTelemetryReporting = () => {
        if (this.heartbeatInterval) {
            clearInterval(this.heartbeatInterval);
        }
        this.telemetryPersistenceManager.disconnect();
    };
    setupEventBusListeners = () => {
        this.options.eventBus.subscribe(RPCEventTopics.handler_call_success, (payload) => {
            this.statistics.handlerSuccessCount++;
        });
        this.options.eventBus.subscribe(RPCEventTopics.handler_error, (payload) => {
            this.statistics.handlerErrorCount++;
        });
        this.options.eventBus.subscribe(RPCEventTopics.rpc_server_error, (payload) => {
            this.statistics.rpcServerErrorCount++;
        });
    };
    saveHeartbeat = async () => {
        return this.telemetryPersistenceManager.saveHeartbeat(this.heartbeatData);
    };
    saveStatistics = () => {
        this.telemetryPersistenceManager.saveStatistics(this.statistics);
    };
}

const debug = Debug__default["default"]('rpc:RPCServer');
class RPCServer {
    eventBus = new EventBus();
    static events = {
        ...RPCEventTopics,
    };
    callManager;
    config;
    httpServer;
    serverStarted = false;
    telemetryManager;
    websocketServer;
    constructor(config) {
        if (!config.ephemeralId) {
            config.ephemeralId = new ObjectId__default["default"]().toString();
        }
        this.config = deepFreeze(config);
        if (!config.displayName) {
            throw new Error('config.displayName is required');
        }
        this.callManager = new CallManager({
            config: this.config,
            eventBus: this.eventBus,
        });
        if (config.telemetry) {
            this.telemetryManager = new TelemetryInfoManager({
                config: this.config,
                eventBus: this.eventBus,
            });
        }
        debug(`RPCServer id: ${this.config.ephemeralId}`);
        this.setupGatewayServer();
    }
    call = async (request, traceCaller) => {
        if (!traceCaller) {
            throw Error('RPCServer.call() requires "traceCaller" param string to track where this call originated');
        }
        const response = await this.callManager.manageRequest(CallRequest.fromCallRequestDTO(request, {
            trace: {
                caller: `RPCServer.call ${traceCaller}`,
                internal: true,
            },
        }));
        if (!response.success) {
            throw response;
        }
        return response;
    };
    getRegisteredHandlers = () => this.callManager.getRegisteredHandlers();
    on = (topic, cb) => {
        if (Object.keys(RPCEventTopics).includes(topic)) {
            const { unsubscribe } = this.eventBus.subscribe(topic, cb);
            return () => unsubscribe();
        }
        else {
            throw new Error(`Error topic "${topic}" not recognized`);
        }
    };
    registerHandler = (request, handler) => {
        if (this.serverStarted) {
            throw Error('RPCServer: you cannot register a handler after the server has started.');
        }
        const wrappedHandler = async (request) => {
            try {
                let callResponse;
                const originalRequest = request;
                const reqKey = makeHandlerRequestKey(originalRequest);
                debug(`wrappedHandler, Handler -> ${reqKey}`, {
                    args: request.args,
                    identity: request.identity,
                    trace: request.trace,
                });
                const internalCallWithCallerIdentity = (request) => {
                    return this.call({ identity: originalRequest.identity, ...request }, makeHandlerRequestKey(originalRequest));
                };
                const handlerResponse = await handler(request, internalCallWithCallerIdentity);
                if (handlerResponse instanceof CallResponse) {
                    callResponse = handlerResponse;
                }
                else if (typeof handlerResponse === 'object' &&
                    handlerResponse.hasOwnProperty('code') &&
                    handlerResponse.hasOwnProperty('success')) {
                    callResponse = new CallResponse(handlerResponse, request);
                }
                else {
                    debug(`ERROR: handler did not return valid POJO or CallResponse - ${request.scope || ''}::${request.procedure || ''}::${request.version || ''}`);
                    throw new Error('handler did not return a valid CallResponse or POJO');
                }
                if (!callResponse.success) {
                    throw callResponse;
                }
                return callResponse;
            }
            catch (e) {
                debug(`wrappedHandler, caught error for request ${makeHandlerRequestKey(request)}`, e);
                if (e instanceof CallResponse) {
                    throw e;
                }
                this.eventBus.publish({
                    payload: {
                        error: e,
                        request,
                        response: undefined,
                    },
                    topic: RPCEventTopics.handler_error,
                });
                console.log('RPCServer wrappedHandler errror: ', e, request);
                return new CallResponse({ code: 500, message: 'server handler issue', success: false }, request);
            }
        };
        this.callManager.registerHandler(request, wrappedHandler);
        this.telemetryManager?.registerHandler(request);
    };
    start = () => {
        if (!this.serverStarted) {
            this.serverStarted = true;
            this?.httpServer?.start();
            this?.websocketServer?.start();
            this?.telemetryManager?.startTelemetryReporting();
        }
        else {
            throw Error('RPCServer: cannot call start() more than once');
        }
    };
    stop = () => {
        this?.httpServer?.stop();
        this?.websocketServer?.stop();
    };
    setupGatewayServer = () => {
        const config = this.config;
        if (!config?.messageBroker && !config.gatewayServer) {
            debug('no config.messageBroker or config.gatewayServer options, acting as standalone server');
        }
        if (config?.gatewayServer?.http) {
            this.httpServer = new HttpServer$1({
                config: this.config,
                eventBus: this.eventBus,
            });
            this.httpServer.setIncomingRequestHandler(this.callManager.manageRequest);
        }
        if (config?.gatewayServer?.websocket) {
            this.websocketServer = new WebSocketServer({
                config: this.config,
                eventBus: this.eventBus,
            });
            this.websocketServer.setIncomingRequestHandler(this.callManager.manageRequest);
        }
    };
}

const DEFAULT_BIND = '127.0.0.1';
class HttpServer {
    options;
    koaApp;
    koaAppListener;
    staticFilesPath;
    telemetryPersistenceManager;
    constructor(options) {
        this.options = options;
        this.koaApp = new Koa__default["default"]();
        this.koaApp.use(cors__default["default"]());
        this.koaApp.use(logger__default["default"]());
        this.staticFilesPath = this.options.config?.server?.staticFilesPath;
        this.telemetryPersistenceManager = options.telemetryPersistenceManager;
    }
    start = (onStartedCallback) => {
        if (this.koaAppListener)
            return;
        this.configureRoutes();
        const host = this.options.config.server?.bind || DEFAULT_BIND;
        const port = this.options.config?.server.port;
        this.koaAppListener = this.koaApp.listen({
            host,
            port,
        }, () => {
            this.log(`Telemetry HTTP server listening ${host}:${port}`);
            if (onStartedCallback) {
                onStartedCallback();
            }
        });
    };
    stop = (onStoppedCallback) => {
        const host = this.options.config.server?.bind || DEFAULT_BIND;
        const port = this.options.config.server.port;
        this.koaAppListener?.close((err) => {
            if (err) ;
            else {
                this.log(`server stopped (${host}:${port})`);
                this.koaAppListener = undefined;
                if (onStoppedCallback) {
                    onStoppedCallback();
                }
            }
        });
    };
    configureRoutes = () => {
        if (this.staticFilesPath) {
            this.koaApp.use(koaStatic__default["default"](this.staticFilesPath));
        }
        this.koaApp.use(async (ctx, next) => {
            switch (ctx.request.path) {
                case '/docs':
                    try {
                        const docs = await this.telemetryPersistenceManager.getProcedureDocs();
                        ctx.body = docs;
                        ctx.status = 200;
                    }
                    catch (e) {
                        console.log('HttpServer error fetching procedure docs:', e);
                        ctx.status = 500;
                    }
                    break;
                case '/health':
                    ctx.body = 'ok';
                    ctx.status = 200;
                    break;
                case '/heartbeats':
                    try {
                        const docs = await this.telemetryPersistenceManager.getHeartbeats();
                        ctx.body = docs;
                        ctx.status = 200;
                    }
                    catch (e) {
                        console.log('HttpServer error fetching heartbeats:', e);
                        ctx.status = 500;
                    }
                    break;
                default:
                    if (this.staticFilesPath) {
                        return koaSendFile__default["default"](ctx, path__default["default"].join(this.staticFilesPath, 'index.html'));
                    }
                    else {
                        ctx.body = 'Route not recognized and no files found';
                        ctx.status = 404;
                    }
                    break;
            }
            return;
        });
    };
    log = (...args) => {
        console.log(`TelemetryServer `, ...args);
    };
}

class TelemetryServer {
    eventBus = new EventBus();
    config;
    telemetryHttpServer;
    telemetryPersistenceManager;
    constructor(config) {
        this.config = deepFreeze(config);
        this.telemetryPersistenceManager = new TelemetryPersistenceManager({
            mongoURI: config.adapter.mongoURI,
        });
        this.telemetryHttpServer = new HttpServer({
            config: config,
            telemetryPersistenceManager: this.telemetryPersistenceManager,
        });
    }
    start = async () => {
        this?.telemetryHttpServer?.start();
    };
    stop = () => {
        this.telemetryPersistenceManager.disconnect();
        this?.telemetryHttpServer?.stop();
    };
}

exports.CallRequest = CallRequest;
exports.CallResponse = CallResponse;
exports.RPCServer = RPCServer;
exports.TelemetryServer = TelemetryServer;
//# sourceMappingURL=index.js.map
